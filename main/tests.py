from django.test import TransactionTestCase
from django.test import override_settings
from django.db import transaction
from rest_framework import status
from rest_framework.test import APIClient
from django import urls
from .models import BankAccount, Transaction, BankAccountLockException
from django.contrib.auth import get_user_model

User = get_user_model()

FIRST_CONNECTION = "default"
SECOND_CONNECTION = "alternate"

DEFAULT_PASSWORD = "mypassword"
DEFAULT_BALANCE = 500
DEFAULT_AMOUNT = 150
DEFAULT_IDEMPOTENCY_KEY = "15f8d9241d113f08"

URL_TRANSACTION = urls.reverse("transaction")


class BankTest(TransactionTestCase):
    databases = {FIRST_CONNECTION, SECOND_CONNECTION}

    def setUp(self):
        self.user1 = User.objects.create_user(
            "user1", "user1@test.com", DEFAULT_PASSWORD
        )
        self.user2 = User.objects.create_user(
            "user2", "user2@test.com", DEFAULT_PASSWORD
        )
        self.user1_bank_account1_id = BankAccount.objects.create(
            user=self.user1, balance=DEFAULT_BALANCE
        ).id
        self.user2_bank_account1_id = BankAccount.objects.create(
            user=self.user2, balance=DEFAULT_BALANCE
        ).id
        self.client1 = APIClient()
        self.client1.force_authenticate(user=self.user1)
        self.client1.credentials(HTTP_IDEMPOTENCY_KEY=DEFAULT_IDEMPOTENCY_KEY)
        self.client2 = APIClient()
        self.client2.credentials(HTTP_IDEMPOTENCY_KEY=DEFAULT_IDEMPOTENCY_KEY)
        self.client2.force_authenticate(user=self.user2)

    def test_transaction_success(self):
        resp = self.client1.post(
            URL_TRANSACTION,
            {
                "sender_account": self.user1_bank_account1_id,
                "recipient_account": self.user2_bank_account1_id,
                "amount": DEFAULT_AMOUNT,
            },
            format="json",
        )

        assert resp.status_code == status.HTTP_200_OK
        assert (
            BankAccount.objects.get(id=self.user1_bank_account1_id).balance
            == DEFAULT_BALANCE - DEFAULT_AMOUNT
        )
        assert (
            BankAccount.objects.get(id=self.user2_bank_account1_id).balance
            == DEFAULT_BALANCE + DEFAULT_AMOUNT
        )
        assert Transaction.objects.count() == 1

    def test_transaction_fails_without_auth(self):
        self.client1.force_authenticate()
        resp = self.client1.post(URL_TRANSACTION)

        assert resp.status_code == status.HTTP_403_FORBIDDEN

    def test_transaction_fails_invalid_cred(self):
        resp = self.client1.post(
            URL_TRANSACTION,
            {
                "sender_account": self.user2_bank_account1_id,
                "recipient_account": self.user1_bank_account1_id,
                "amount": DEFAULT_AMOUNT,
            },
            format="json",
        )

        assert resp.json() == {
            "sender_account": [
                "The specified account of the sender does not belong to the user"
            ]
        }
        assert resp.status_code == status.HTTP_400_BAD_REQUEST

    def test_transaction_fails_not_enough_money(self):
        resp = self.client1.post(
            URL_TRANSACTION,
            {
                "sender_account": self.user1_bank_account1_id,
                "recipient_account": self.user2_bank_account1_id,
                "amount": 500000,
            },
            format="json",
        )

        assert resp.status_code == status.HTTP_400_BAD_REQUEST
        assert resp.json() == {"Error": "Not enough funds in the account"}

    def test_transaction_success_idempotency(self):
        request_data = {
            "sender_account": self.user1_bank_account1_id,
            "recipient_account": self.user2_bank_account1_id,
            "amount": DEFAULT_AMOUNT,
        }
        resp1 = self.client1.post(
            URL_TRANSACTION, request_data, format="json"
        )
        resp2 = self.client1.post(
            URL_TRANSACTION, request_data, format="json"
        )

        assert resp1.status_code == status.HTTP_200_OK
        assert resp2.status_code == status.HTTP_200_OK
        assert (
            BankAccount.objects.get(id=self.user1_bank_account1_id).balance
            == DEFAULT_BALANCE - DEFAULT_AMOUNT
        )
        assert (
            BankAccount.objects.get(id=self.user2_bank_account1_id).balance
            == DEFAULT_BALANCE + DEFAULT_AMOUNT
        )
        assert Transaction.objects.count() == 1

        self.client1.credentials(HTTP_IDEMPOTENCY_KEY="new_idempotency")
        resp3 = self.client1.post(
            URL_TRANSACTION, request_data, format="json"
        )

        assert resp3.status_code == status.HTTP_200_OK
        assert (
            BankAccount.objects.get(id=self.user1_bank_account1_id).balance
            == DEFAULT_BALANCE - DEFAULT_AMOUNT * 2
        )
        assert (
            BankAccount.objects.get(id=self.user2_bank_account1_id).balance
            == DEFAULT_BALANCE + DEFAULT_AMOUNT * 2
        )
        assert Transaction.objects.count() == 2

    @override_settings(BANK_ACCOUNT_LOCK_NOWAIT=True)
    def test_transaction_parallel_raises_exception_bank_account_in_progress(self):
        with transaction.atomic(using=FIRST_CONNECTION):
            Transaction.transfer(
                self.user1_bank_account1_id,
                self.user2_bank_account1_id,
                DEFAULT_AMOUNT,
                DEFAULT_IDEMPOTENCY_KEY,
                FIRST_CONNECTION,
            )

            with transaction.atomic(using=SECOND_CONNECTION):
                with self.assertRaises(BankAccountLockException):
                    Transaction.transfer(
                        self.user2_bank_account1_id,
                        self.user1_bank_account1_id,
                        DEFAULT_AMOUNT,
                        DEFAULT_IDEMPOTENCY_KEY,
                        SECOND_CONNECTION,
                    )

    def test_get_top_transactions(self):
        user1_bank_account2_id = BankAccount.objects.create(
            user=self.user1, balance=DEFAULT_BALANCE
        ).id
        user2_bank_account2_id = BankAccount.objects.create(
            user=self.user2, balance=DEFAULT_BALANCE
        ).id

        Transaction.transfer(
            self.user1_bank_account1_id,
            user1_bank_account2_id,
            300,
            DEFAULT_IDEMPOTENCY_KEY,
        )
        Transaction.transfer(
            self.user1_bank_account1_id,
            user2_bank_account2_id,
            100,
            DEFAULT_IDEMPOTENCY_KEY,
        )
        Transaction.transfer(
            self.user2_bank_account1_id,
            user2_bank_account2_id,
            400,
            DEFAULT_IDEMPOTENCY_KEY,
        )

        resp = self.client1.get(urls.reverse("transaction-top"))

        assert resp.status_code == status.HTTP_200_OK
        resp_json = resp.json()
        assert resp_json[0]["user_id"] == self.user2.id
