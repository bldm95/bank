from django.contrib import admin

from .models import Transaction, BankAccount


@admin.register(Transaction)
class TransactionAdmin(admin.ModelAdmin):
    ...


@admin.register(BankAccount)
class BankAccountAdmin(admin.ModelAdmin):
    ...
