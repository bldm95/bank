from django.urls import path

import main.views

urlpatterns = [
    path("transaction/", main.views.TransactionView.as_view(), name="transaction"),
    path("transaction-top/", main.views.TransactionTopView.as_view(), name="transaction-top"),
]
