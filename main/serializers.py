from rest_framework import serializers

from .models import Transaction


class TransactionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transaction
        fields = ("sender_account", "recipient_account", "amount")

    def validate_sender_account(self, sender_account):
        user = self.context["request"].user
        if sender_account.user.id != user.id:
            raise serializers.ValidationError(
                "The specified account of the sender does not belong to the user"
            )
        return sender_account

    def validate(self, attrs):
        if attrs["sender_account"].id == attrs["recipient_account"].id:
            raise serializers.ValidationError(
                "The sender's account and the recipient's account cannot be the same"
            )
        return attrs


class TransactionTopSerializer(serializers.ModelSerializer):
    user_id = serializers.SerializerMethodField()

    class Meta:
        model = Transaction
        fields = ("sender_account", "recipient_account", "user_id", "amount")

    @staticmethod
    def get_user_id(obj):
        return obj.sender_account.user.id
