from django.conf import settings
from django.contrib.auth import get_user_model
from django.db import models, transaction, OperationalError, IntegrityError
from rest_framework.exceptions import ValidationError

User = get_user_model()

BANK_ACCOUNT_LOCK_NOWAIT = getattr(settings, "BANK_ACCOUNT_LOCK_NOWAIT", False)


class BankAccount(models.Model):
    user = models.ForeignKey(User, null=False, blank=False, on_delete=models.PROTECT)
    balance = models.DecimalField(max_digits=10, decimal_places=2, default=0)


class BankAccountLockException(Exception):
    ...


class Transaction(models.Model):
    sender_account = models.ForeignKey(
        BankAccount,
        related_name="transfer_sender",
        null=False,
        blank=False,
        on_delete=models.PROTECT,
    )
    recipient_account = models.ForeignKey(
        BankAccount,
        related_name="transfer_recipient",
        null=False,
        blank=False,
        on_delete=models.PROTECT,
    )
    amount = models.DecimalField(
        max_digits=10, decimal_places=2, null=False, blank=False
    )
    idempotency_key = models.CharField(max_length=16, null=False, blank=False)

    class Meta:
        unique_together = ["sender_account", "recipient_account", "idempotency_key"]

    @staticmethod
    def transfer(
        sender_account_id, recipient_account_id, amount, idempotency_key, db="default"
    ):
        with transaction.atomic(using=db):
            try:
                sender_account = (
                    BankAccount.objects.using(db)
                    .select_for_update(BANK_ACCOUNT_LOCK_NOWAIT)
                    .get(id=sender_account_id)
                )
                recipient_account = (
                    BankAccount.objects.using(db)
                    .select_for_update(BANK_ACCOUNT_LOCK_NOWAIT)
                    .get(id=recipient_account_id)
                )
            except OperationalError:
                raise BankAccountLockException

            if sender_account.balance < amount:
                raise ValidationError({"Error": "Not enough funds in the account"})
            try:
                Transaction.objects.using(db).create(
                    sender_account_id=sender_account_id,
                    recipient_account_id=recipient_account_id,
                    amount=amount,
                    idempotency_key=idempotency_key,
                )
            except IntegrityError:
                return

            sender_account.balance -= amount
            recipient_account.balance += amount
            sender_account.save()
            recipient_account.save()

        return
