from django.contrib.auth import get_user_model
from django.db.models import F
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import generics, status

from main import serializers
from main.models import Transaction

User = get_user_model()


class TransactionView(generics.GenericAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = serializers.TransactionSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        Transaction.transfer(
            serializer.validated_data["sender_account"].id,
            serializer.validated_data["recipient_account"].id,
            serializer.validated_data["amount"],
            request.META.get("HTTP_IDEMPOTENCY_KEY"),
        )

        return Response("Successful", status=status.HTTP_200_OK)


class TransactionTopView(generics.ListAPIView):
    serializer_class = serializers.TransactionTopSerializer

    def get_queryset(self):
        pks = (
            Transaction.objects.filter(
                sender_account__user=F("recipient_account__user")
            )
            .order_by("sender_account__user", "-amount")
            .distinct("sender_account__user")
        ).values_list("pk", flat=True)

        return (
            Transaction.objects.filter(pk__in=pks)
            .select_related("sender_account__user")
            .order_by("-amount")
        )
