FROM python:3.6

ENV PYTHONUNBUFFERED 1
RUN pip install "poetry==1.0.0b9"
RUN mkdir /code
WORKDIR /code

# Poetry
RUN poetry config virtualenvs.create false
COPY poetry.lock pyproject.toml /code/
RUN poetry install --no-interaction

COPY . /code/