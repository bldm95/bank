# Bank

Install dependencies:
`poetry install`

Activate virtual environment: `poetry shell`

## Docker 

**Install Docker and docker-compose on your operating system**

Run build the app:
`sudo docker-compose up -d`

Apply migrations:
`sudo docker-compose run django python /code/manage.py migrate --noinput`

Createsuperuser:
`sudo docker-compose run django python /code/manage.py createsuperuser`

Run tests:
`sudo docker-compose run django python /code/manage.py test --settings=bank.test_settings`